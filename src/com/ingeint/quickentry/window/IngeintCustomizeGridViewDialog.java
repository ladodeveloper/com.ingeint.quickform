package com.ingeint.quickentry.window;

import java.util.ArrayList;
import java.util.Map;

import org.adempiere.webui.adwindow.GridView;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.panel.CustomizeGridViewPanel;
import org.adempiere.webui.window.CustomizeGridViewDialog;
import org.compiere.util.Env;

import com.ingeint.quickentry.forms.QuickGridView;
import com.ingeint.quickentry.webui.panel.QuickCustomizeGridViewPanel;

public class IngeintCustomizeGridViewDialog extends CustomizeGridViewDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7630571240649839544L;
	
	private CustomizeGridViewPanel customizePanel;
	private QuickCustomizeGridViewPanel quickCustomizePanel;

	public IngeintCustomizeGridViewDialog(int windowNo, int AD_Tab_ID, int AD_User_ID,
			Map<Integer, String> columnsWidth, ArrayList<Integer> gridFieldIds, boolean isQuickForm2) {
		super(windowNo, AD_Tab_ID, AD_User_ID, columnsWidth, gridFieldIds);
		// TODO Auto-generated constructor stub
	}



	private boolean isQuickForm = false;
	
	private void initComponent(int windowNo, int AD_Tab_ID, int AD_User_ID, Map<Integer, String> columnsWidth,
			ArrayList<Integer> gridFieldIds) {

		if (isQuickForm)
		{
			quickCustomizePanel = new QuickCustomizeGridViewPanel(windowNo, AD_Tab_ID, AD_User_ID, columnsWidth,
					gridFieldIds);
		}
		else
		{
			customizePanel = new CustomizeGridViewPanel(windowNo, AD_Tab_ID, AD_User_ID, columnsWidth, gridFieldIds);
		}
		
		this.setStyle("position : absolute;");
		this.setBorder("normal");
		this.setSclass("popup-dialog");
		
		if (isQuickForm)
		{
			this.setWidth("500px");
			this.setHeight("410px");
			quickCustomizePanel.createUI();
			quickCustomizePanel.loadData();
			appendChild(quickCustomizePanel);
		}
		else
		{
			this.setWidth("600px");
			this.setHeight("500px");
			appendChild(customizePanel);
			customizePanel.createUI();
			customizePanel.query();
		}
	}
	
	
	/**
	 * Show User customize (modal)
	 * @param WindowNo window no
	 * @param AD_Tab_ID
	 * @param columnsWidth 
	 * @param gridFieldIds list fieldId current display in gridview
	 * @param gridPanel
	 * @param isQuickForm
	 * @param quickGridView 
	 */
	public static boolean showNewCustomize(int WindowNo, int AD_Tab_ID, Map<Integer, String> columnsWidth,
			ArrayList<Integer> gridFieldIds, GridView gridPanel, QuickGridView quickGridView, boolean isQuickForm)
	{
		IngeintCustomizeGridViewDialog customizeWindow = new IngeintCustomizeGridViewDialog(WindowNo, AD_Tab_ID,
				Env.getAD_User_ID(Env.getCtx()), columnsWidth, gridFieldIds, isQuickForm);
		if (isQuickForm)
		{
			customizeWindow.setquickGridView(quickGridView);
		}
		else
		{
			customizeWindow.setGridPanel(gridPanel);
		}
		AEnv.showWindow(customizeWindow);
		return customizeWindow.isSaved();
	} // showProduct
	
	/**
	 * @return whether change have been successfully save to db
	 */
	public boolean isSaved() {
		boolean isSaved;
		if (isQuickForm)
			isSaved = quickCustomizePanel.isSaved();
		else
			isSaved = customizePanel.isSaved();
		return isSaved;
	}

	/**
	 * @param QuickGridView
	 */
	private void setquickGridView(QuickGridView quickGridView)
	{
		quickCustomizePanel.setGridPanel(quickGridView);
	}
	
	

	
	
	
	
	
	
	

}
