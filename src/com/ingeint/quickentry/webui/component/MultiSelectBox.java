package com.ingeint.quickentry.webui.component;


import java.util.ArrayList;

import org.adempiere.webui.LayoutUtils;
import com.ingeint.quickentry.webui.component.WMultiSelectEditor;
import org.compiere.util.CacheMgt;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.util.DesktopCleanup;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;

/**
 * Layout for handle multiple selection items ( IDEMPIERE-3413 )
 * 
 * @author Logilite Technologies
 * @since June 28, 2017
 */
public class MultiSelectBox extends Div
{

	/**
	 * 
	 */
	private static final long	serialVersionUID		= -632973531627268781L;

	public static final String	ATTRIBUTE_MULTI_SELECT	= "ATTRIBUTE_MULTI_SELECT";

	public WMultiSelectEditor	editor;
	protected DesktopCleanup	listener				= null;

	private Textbox				textbox;
	private Popup				popup;
	private Vbox				vbox;
	private boolean				editable;

	private ArrayList<Checkbox>	cbxList					= new ArrayList<Checkbox>();

	public MultiSelectBox()
	{
		super();
		this.setWidth("100%");
		init();
	}

	private void init()
	{
		LayoutUtils.addSclass(".multi-select-box", this);

		popup = new Popup();
		LayoutUtils.addSclass("multi-select-popup", popup);
		appendChild(popup);

		vbox = new Vbox();
		vbox.setHflex("1");
		LayoutUtils.addSclass("multi-select-vbox", vbox);
		popup.appendChild(vbox);

		textbox = new Textbox("") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1567895599198729743L;

			public void setValue(String value) throws WrongValueException
			{
				super.setValue(value);
				setTooltiptext(value);
			}
		};

		if (isEnabled())
			textbox.setPopup("uuid(" + popup.getUuid() + "), after_start");

		textbox.setReadonly(true);
		textbox.setHflex("1");
		LayoutUtils.addSclass("multi-select-textbox", textbox);
		appendChild(textbox);
	} // init

	public Popup getPopupComponent()
	{
		return popup;
	}

	public Textbox getTextbox()
	{
		return textbox;
	}

	public ArrayList<Checkbox> getCheckboxList()
	{
		return cbxList;
	}

	public Vbox getVBox()
	{
		return vbox;
	}

	public boolean isEnabled()
	{
		return editable;
	}

	public void setEditable(boolean editable)
	{
		this.editable = editable;

		if (textbox != null)
		{
			if (!isEnabled())
			{
				LayoutUtils.addSclass("multi-select-textbox-readonly", textbox);
				textbox.setPopup((Popup) null);
			}
			else
			{
				LayoutUtils.addSclass("multi-select-textbox", textbox);
				textbox.setPopup("uuid(" + popup.getUuid() + "), after_start");
			}
		}
	} // setEditable

	@Override
	public void onPageAttached(Page newpage, Page oldpage)
	{
		super.onPageAttached(newpage, oldpage);
		if (editor.tableCacheListener == null)
		{
			editor.createCacheListener();
			if (listener == null)
			{
				listener = new DesktopCleanup() {
					@Override
					public void cleanup(Desktop desktop) throws Exception
					{
						MultiSelectBox.this.cleanup();
					}
				};
				newpage.getDesktop().addListener(listener);
			}
		}
	} // onPageAttached

	@Override
	public void onPageDetached(Page page)
	{
		super.onPageDetached(page);
		if (listener != null && page.getDesktop() != null)
			page.getDesktop().removeListener(listener);
		cleanup();
	} // onPageDetached

	protected void cleanup()
	{
		if (editor.tableCacheListener != null)
		{
			CacheMgt.get().unregister(editor.tableCacheListener);
			editor.tableCacheListener = null;
		}
	} // cleanup
}
