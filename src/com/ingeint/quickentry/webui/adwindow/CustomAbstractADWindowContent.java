package com.ingeint.quickentry.webui.adwindow;

import java.util.Properties;

import org.adempiere.webui.adwindow.AbstractADWindowContent;
import org.adempiere.webui.adwindow.IADTabbox;
import org.adempiere.webui.adwindow.StatusBar;
import org.zkoss.zk.ui.Component;

import com.ingeint.quickentry.forms.QuickGridView;

public class CustomAbstractADWindowContent extends AbstractADWindowContent {

	public CustomAbstractADWindowContent(Properties ctx, int windowNo, int adWindowId) {
		super(ctx, windowNo, adWindowId);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Component getComponent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected IADTabbox createADTab() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void switchEditStatus(boolean editStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected Component doCreatePart(Component parent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
    
    private void updateToolbarCustom()
	{
		toolbar.enableTabNavigation(breadCrumb.hasParentLink(), adTabbox.getSelectedDetailADTabpanel() != null);

		toolbar.getButton("Attachment").setPressed(adTabbox.getSelectedGridTab().hasAttachment());
		toolbar.getButton("Chat").setPressed(adTabbox.getSelectedGridTab().hasChat());
		toolbar.getButton("Find").setPressed(adTabbox.getSelectedGridTab().isQueryActive());

		if (toolbar.isPersonalLock)
		{
			toolbar.lock(adTabbox.getSelectedGridTab().isLocked());
		}

		toolbar.enablePrint(adTabbox.getSelectedGridTab().isPrinted() && !adTabbox.getSelectedGridTab().isNew());
		
		toolbar.enableCustomize(adTabbox.getSelectedTabpanel().isEnableCustomizeButton());

		toolbar.enableQuickForm(adTabbox.getSelectedTabpanel().isEnableQuickFormButton() && !adTabbox.getSelectedGridTab().isReadOnly());

	}
    
	/**
	 * @return Quick Form StatusBar
	 */
	public StatusBar getStatusBarQF()
	{
		return getStatusBarQF();
	}
	
	/**
	 * Implementation to work key listener for the current opened Quick Form.
	 */
	QuickGridView currQGV = null;

	/**
	 * @return
	 */
	public QuickGridView getCurrQGV()
	{
		return currQGV;
	}

	/**
	 * @param currQGV
	 */
	public void setCurrQGV(QuickGridView currQGV)
	{
		this.currQGV = currQGV;
	}

}
